# pihole-custom

pihole custom list.

## structure

    directory/
        exact-blocklist.txt
        regex-blocklist.txt

`exact-blocklist.txt` can be imported as a host list.

`regex-blocklist.txt` should/cannot as it contains regex, not hosts. not sure how to import them, other than manually inserting each row.

